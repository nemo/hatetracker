---
layout: story
title: About
permalink: /about/
---

[Source](http://www.hindustantimes.com/hate-tracker/about "Permalink to About the project Hindustan Times Hate Tracker"

![][1]

When a mob lynches a Muslim man on the suspicion of possessing beef, it is recorded as a murder. When a North-Eastern or African student is abused with slurs, it is recorded as an obscene act. When a gay man is beaten because of the way he looks and acts, it is recorded as an assault.

In all these situations, violence or abuse may be punished, but bigotry is not. Part of the problem is lack of laws dealing with hate crimes in the Indian legal system. The closest thing we have is [The Scheduled Caste and Scheduled Tribe (Prevention of Atrocities) Act][2], which criminalises a range of activities prejudicial to Dalits and members of scheduled tribes.

This means there is a dearth of information about the power and influence of hate. That, in turn, makes it impossible to know the extent of the problem. There is no national database of hate crimes.

Hindustan Times intends to fill this void. Our Hate Tracker will monitor acts of violence, threats of violence, and incitements to violence based on religion, caste, race, ethnicity, region of origin, gender identity and sexual orientation.

## What we hope the Hate Tracker will achieve

Hate crimes are humiliating and degrading. They are directed at the dignity of a whole community, and make large groups of people feel unsafe and fearful in their homes. Some leave. Their lives are changed forever.

As a part of the series of articles that accompanied the announcement of this website, we reported from Mirchpur, a Haryana village that was witness to a horrific case of caste violence in 2010. After a mob of Jats torched Dalit homes, all of the village's Dalits fled. Most Dalit families have decided not to return. Instead, they have lived in a makeshift camp for seven years.

The conditions are miserable. "Look around at how we have been living," said one resident, gesturing at his tent, "without even a roof over our heads."

Making a public record of these crimes will provide solid, irrefutable evidence to researchers, activists, and political leaders trying to grapple with the challenges of hate. We hope our database will also clarify to general readers the variety and extent of intolerance in India.

## How to identify a hate crime

For the purpose of this project, we are relying on standard international practices for determining what counts as a hate crime. 

These criteria include the following questions. Was the identity of the victim visibly obvious from his or her appearance? Did the accused use slurs or evince knowledge of the victim's identity in some other way? Does the social or political situation of the crime suggest a motive to threaten, incite, or commit violence on the basis of identity? Has the attacker previously expressed hatred for the targeted group? 

The hardest part about determining whether something should count as a hate crime is establishing that the offender was motivated by prejudice. In the United States, the federal government helps with this process by providing investigative and prosecutorial support to local law enforcement and by compiling annual statistics on hate crimes. In the US legal system, a hate crime is punished as an 'enhancement', an extra penalty that is added onto an existing set of charges.

## What we counted

There are too many different sorts of hate crime for us to list all of them. But here are a few varieties that are relatively common across the country.

We consider acts of violence, threats of violence, and incitements to violence in the name of cow vigilantism to be hate crimes, since all such behaviour targets people on the basis of religious belief. Members of any faith, including Hinduism, may be victims. 

We count honour killings in the name of religion or caste as hate attacks. 

Finally, we are also including several forms of humiliation traditionally visited upon Dalits, such as public stripping and tonsuring.

![][3]

## What we did not count

Terrorism tends to be motivated by hate, but it is considered a separate and uniquely dire offence throughout the world.

In the US, for instance, terrorism is defined as "the unlawful use of force or violence to intimidate a government or civilian population for political or social objectives". This aspect of attacking a state or a whole society is generally what distinguishes terrorism from hate crimes.

We have also used this logic in our Hate Tracker, and not included acts of terrorism, though we recognize that the distinction is not obvious in every case. (The BBC analysed some of the legal technicalities involved in [this informative piece][4].)

Like acts of terrorism, attacks that are part of an ongoing political or military conflict are also generally not included as hate crimes.

We have not counted every instance of mob violence as a hate crime. For instance, according to the descriptions of the crime that have been published, the mob that lynched deputy superintendent of police Ayub Pandith was not motivated by the categories of identity relevant to hate crimes. The lynchings in Jharkhand over rumours of child trafficking were also not inspired by the identities of the victims. The death of Zafar Khan, allegedly at the hands of public officials, was brought about not by his identity but by his objections to the photographing of a woman openly defecating. 

Finally, attacks by and between members of two political parties are not included.

## How does the government count hate crimes?

The [National Crime Records Bureau][5] publishes an annual report on crime statistics from every state in India. But there is no separate classification for identity-based hate crimes. The available figures include the number of crimes registered under the SC/ST Act, under section 153A, and as part of communal riots. 

This data set is woefully inadequate. The main problem is the NCRB classification of the 'primary offence'. Each incident may invoke several sections, but the NCRB will record them under only the main offence. A murder motivated by religious prejudice, for example, will appear only as a murder. 

The NCRB list consists mainly of numbers. In our list, we have also included details about the circumstances of each incident, links to reports about it, and the names of anyone who was killed. 

![][6]

## Data sources

The figures and descriptions of our database are drawn from news stories in the English-language press and reports from civil society organizations. 

This information is not final and has not, in many cases, been tried in a court of law. These reports still reflect the factual information currently available on each incident. 

This does not suggest the guilt of individual parties until the judicial process is complete. When courts adjudicate on a case, we will update the Tracker accordingly.

We realize its limitations: not all such incidents get covered by these sources, and we may miss even some of the ones that do. Our figures will underestimate the real number.

To make our list comprehensive, we are working to expand our linguistic capabilities. If you would like to collaborate with us or make any suggestions, please contact us using the information below. 

## Contact us

Please use this form to submit information, links, and reports to all incidents that you see, read, or come across in person.

You can write to us at hatetracker@hindustantimes.com. 

We'd be grateful for documentary evidence of any crimes, such as photos or videos, and we'd also like to hear about errors or further information (especially case updates) on the incidents recorded on our database. 

All information shared with us will be strictly confidential and not shared with anyone. In the submission form on this site, it is not compulsory to include your name, email address, or phone number, so you can give us a tip without revealing who you are. We will publish only the information that we can verify.

## Credits

* Research and reporting: Niha Masih
* Development: Vishrut Kohli, Shivam Kohli, Gurman Bhatia and Balaji Bora Jagannath Naidu
* Design: Gurman Bhatia
* Data: Harry Stevens, Manas Sharma, Samarth Bansal, Gurman Bhatia
* Additional Research: Sujoy Das
* Photographs: Burhaan Kinu
* Videos: Manira Chaudhary, Uzma Mollah, Ummul Baneen, Aastha Purwar, Aayush Bhardwaj
* Animation: Lolita Bhaduri
* Illustrations: Malay Karmakar
* Production: Zehra Kazmi, Dhruba Jyoti
* Editing: Alex Traub

[1]: https://web.archive.org/web/20171024173605im_/http%3A/www.hindustantimes.com/hate-tracker/static/api/img/extra/kashmir.JPG
[2]: https://web.archive.org/web/20171024173605/http://www.indiacode.nic.in/acts-in-pdf/2016/201601.pdf
[3]: https://web.archive.org/web/20171024173605im_/http%3A/www.hindustantimes.com/hate-tracker/static/api/img/extra/politician.JPG
[4]: https://web.archive.org/web/20171024173605/http://www.bbc.com/news/world-us-canada-33205339
[5]: https://web.archive.org/web/20171024173605/http://ncrb.gov.in/
[6]: https://web.archive.org/web/20171024173605im_/http%3A/www.hindustantimes.com/hate-tracker/static/api/img/extra/dadri.JPG